This patch is for nvidia origin/l4t/l4t-r${PATCH_VERSION} branch

How to use:
1) clone original nvidia repository, switch to origin/l4t/l4t-r${PATCH_VERSION}
2) copy firmware file "tegra21x_xusb_firmware" from nvidia driver package (it is appearing in rootfs directory after apply_binaries.sh script execution) to "firmware" kernel directory
3) copy this patch to kernel root directory and apply it:
patch -p1 < jetson-vitcompany-${PATCH_VERSION}.patch
4) build kernel as usual, like this:
ARCH=arm64 make tegra21_vitcompany_defconfig
CROSS32CC=${PATH_TO_32BIT_GCC}/arm-linux-gnueabihf-gcc CROSS_COMPILE=${PATH_TO_64BIT_GCC}/aarch64-linux-gnu- make -j${THREADS_YOU_WANT}

Results:
arch/arm64/boot/Image - kernel image, copy it as /boot/Image on sample rootfs
arch/arm64/boot/dts/tegra210-jetson-cv-vitcompany.dtb - device tree image, copy as /boot/tree.dtb on sample rootfs